package com.sample.vsegura15dev.brujula

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import android.util.AttributeSet
import android.view.View

class CompassView : View {

    private val insidePercentage = 0.95F
    private val borderPercentage = 1 - insidePercentage
    private val trianglePercentage = 0.2F

    private var centerX = 0F
    private var centerY = 0F
    private var triangleWidth = 0F

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    private fun setVariables() {
        this.centerX = width.toFloat() / 2
        this.centerY = height.toFloat() / 2
        this.triangleWidth = width * trianglePercentage
    }

    override fun onDraw(canvas: Canvas) {

        setVariables()

        getCompassBorder().draw(canvas)
        getCompassInside().draw(canvas)

        canvas.drawPath(getUpperTriangle(), Paint().apply {
            color = Color.RED
        })

        canvas.drawPath(getLowerTriangle(), Paint().apply {
            color = Color.GRAY
        })
    }


    private fun getCompassBorder(): ShapeDrawable {

        val compassBorderDrawable = ShapeDrawable(OvalShape())
        compassBorderDrawable.paint.color = Color.GRAY
        compassBorderDrawable.setBounds(0, 0, width, height)

        return compassBorderDrawable
    }

    private fun getCompassInside(): ShapeDrawable {

        val insideWidth = (width) * insidePercentage
        val insideHeight = (height) * insidePercentage

        val insideX = (width * borderPercentage) / 2
        val insideY = (height * borderPercentage) / 2

        val compassBorderDrawable = ShapeDrawable(OvalShape())
        compassBorderDrawable.paint.color = Color.BLUE
        compassBorderDrawable.setBounds(
            insideX.toInt(), insideY.toInt(), (insideX + insideWidth).toInt(), (insideY + insideHeight).toInt())

        return compassBorderDrawable
    }

    private fun getUpperTriangle(): Path {

        val insideCompassY = (height * borderPercentage) / 2

        val path = Path()
        path.moveTo(centerX - (triangleWidth / 2), centerY)
        path.lineTo(centerX, insideCompassY)
        path.lineTo(centerX + (triangleWidth / 2), centerY)
        path.lineTo(centerX - (triangleWidth / 2), centerY)
        path.close()

        return path
    }

    private fun getLowerTriangle(): Path {

        val insideCompassY = (height * borderPercentage) / 2
        val insideHeight = (height) * insidePercentage

        val path = Path()
        path.moveTo(centerX - (triangleWidth / 2), centerY)
        path.lineTo(centerX, (insideCompassY + insideHeight))
        path.lineTo(centerX + (triangleWidth / 2), centerY)
        path.lineTo(centerX - (triangleWidth / 2), centerY)
        path.close()

        return path
    }


}