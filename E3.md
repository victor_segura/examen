# Pregunta 3

    Explica cómo organizas en base a tu experiencia un proyecto en Android utilizando MVP e implementando Clean Architecture, menciona los paquetes que utilizarías y la distribución de módulos.

 Organizaría el proyecto de la siguiente forma:

* Vista

    Esta capa contiene las interfaces que contemplan las funciones o habilidades se puede hacer en la UI. Estas se implementan en los activity o en el fragment.Estos interactuan con la capa presenter
    Contiene los siguientes componentes:

    * View
    * Activity
    * Fragment

* Presenter

    Tiene cómo objetivo definir la lógica de la interfaz. El presenter será quien decida en que momento y que acción podrá realizar la vista.
    Sólo contiene la interfaz de los presenter y su implementación. Estos interactuan con la capa servicio y con las interfaces de la Vista

* Model 

    * Service

        En esta capa lo uso para implementar cierta lógica de negocio o para  acceder a recursos externos cómo un servicio web o una base de datos interna, o a los preferences. Usando una interfaz por cada servicio, en general, uso dos implementaciones, una para que consulta a un servicio web y el otro para acceder a la base de datos. 

    * Database

        Acá defino las clases y interfaces que configurar el acceso de la base de datos. Por ejemplo, uso de DAO y la configuración de la base de datos.

    * Model

        Son las clases que definen las estructura de información.  

* UI, esta capa contiene algunos componentes de la interfaz de usuario.
    
    * Adapter

      Aquí se ubican los adapters personalizados que se usa un RecyclerView o otro ListView o afines

    * Dialog

      Aquí se ubican los dialogos personalizados. 

    * Etc 

      En caso de customizar cualquier otro componente de vista, lo agrego en esta carpeta.     
    
Cabe mencionar que estas capas sólo interacturarian con sus capas contiguas. Es decir, por ejemplo, la capa vista interactua solamente con la capa presenter, y este a su vez, interactua con la capa de vista y de service. Adicionalmente, si una clase o instancia usa hace referencia a una clase de la capa contigua, sólo conoce la interface pero no su implementación de manera que si deseamos agregar o reemplazar una nueva implementación, las clases participantes no se ven afectadas. 

Usando el package base cómo com.vseguradev.app, tendria los siguiente:

* com.vseguradev.app.view
    * .activity
    * .fragment
* com.vseguradev.app.presenter
* com.vseguradev.app.service
* com.vseguradev.app.database
    * .dao
    * .configuration
* com.vseguradev.app.model  
* com.vseguradev.app.ui
    * .adapter
    * .dialog

