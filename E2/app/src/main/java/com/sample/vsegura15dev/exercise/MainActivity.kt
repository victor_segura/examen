package com.sample.vsegura15dev.exercise

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.view.ViewGroup
import android.widget.ScrollView
import android.widget.TextView
import androidx.core.app.ActivityCompat
import com.sample.vsegura15dev.exercise.shake.OpenCameraShakeService
import com.sample.vsegura15dev.exercise.shake.ShakeService
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    private val cameraRequestPermissionCode = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val shakeServiceIntent = Intent(this, OpenCameraShakeService::class.java)
        startService(shakeServiceIntent)
        requestPermissionButton.setOnClickListener {
            goToPermissionSettings()
        }

    }

    override fun onStart() {
        super.onStart()
        requestPermissionForCamera()
    }


    private fun requestPermissionForCamera() {

        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), cameraRequestPermissionCode)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == cameraRequestPermissionCode && grantResults.first() == PackageManager.PERMISSION_GRANTED) {
            requestPermissionButton.visibility = View.INVISIBLE
            messageTextView.text = getString(R.string.open_camera_message)
        } else {
            requestPermissionButton.visibility = View.VISIBLE
            messageTextView.text = getString(R.string.request_refused_message)
        }
    }

    private fun goToPermissionSettings() {

        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        val uri = Uri.fromParts("package", packageName, null)
        intent.data = (uri)
        startActivity(intent)
    }

}
