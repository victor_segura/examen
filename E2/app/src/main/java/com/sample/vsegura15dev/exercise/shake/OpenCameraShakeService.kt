package com.sample.vsegura15dev.exercise.shake

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import androidx.core.content.ContextCompat

class OpenCameraShakeService : ShakeService() {

    override fun executeActionByShake() {

        if (ContextCompat.checkSelfPermission(baseContext, Manifest.permission.CAMERA)
            != PackageManager.PERMISSION_DENIED)
            openCamera()

    }

    private fun openCamera() {

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent, Bundle())

    }
}