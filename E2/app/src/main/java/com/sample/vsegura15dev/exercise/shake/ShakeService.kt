package com.sample.vsegura15dev.exercise.shake

import android.app.Service
import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Handler
import android.os.IBinder

abstract class ShakeService : Service(), SensorEventListener {

    private lateinit var sensorManager: SensorManager
    private lateinit var accelerometerSensor: Sensor
    private var minimumShakeAcceleration = 5
    var acceleration = 0f
    var currentAcceleration = 0f
    var lastAcceleration = 0f


    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        sensorManager.registerListener(this, accelerometerSensor, SensorManager.SENSOR_DELAY_UI, Handler())

        return START_STICKY
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    override fun onSensorChanged(sensor: SensorEvent?) {

        val vectorX = sensor!!.values[0].toDouble()
        val vectorY = sensor.values[1].toDouble()
        val vectorZ = sensor.values[2].toDouble()

        lastAcceleration = currentAcceleration

        currentAcceleration = Math.sqrt(vectorX * vectorX + vectorY * vectorY + vectorZ * vectorZ).toFloat()

        val delta = currentAcceleration - lastAcceleration

        acceleration = acceleration * 0.9f + delta

        if (acceleration > minimumShakeAcceleration) {
            executeActionByShake()
        }
    }

    abstract fun executeActionByShake()
}